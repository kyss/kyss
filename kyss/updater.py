########################################################################
#     ┏┓┏━┓━━━━━━━━━━━━━
#     ┃┃┃┏┛━━━━━━━━━━━━━
#     ┃┗┛┛━┏┓━┏┓┏━━┓┏━━┓
#     ┃┏┓┃━┃┃━┃┃┃━━┫┃━━┫
#     ┃┃┃┗┓┃┗━┛┃┣━━┃┣━━┃
#     ┗┛┗━┛┗━┓┏┛┗━━┛┗━━┛
#     ━━━━━┏━┛┃━━━━━━━━━
#     ━━━━━┗━━┛━━━━━━━━━
#
# This file is part of the Kyss project (https://gitlab.com/ulukyn/kyss).
# Copyright (c) 2020 Nuno Gonçalves (Ulukyn).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import os
import re
import sys
import stat
import glob
import shutil
import hashlib
import zipfile
import platform
import importlib
import subprocess
import configparser

from packaging.version import Version
from io import BytesIO

from pathlib import Path
from kyss.module import KyssModule
from kyss.utils import KyssUtils
from .utils import *

k = KyssUtils("KyssInstaller")

class KyssUpdater(KyssModule):

	def getUsedZones(self):
		return {"footer": 1}

	def _pythonPath(self):
		if platform.system() == "Linux":
			return "/python/opt/python"+python_version+"/lib/python"+python_version+"/site-packages"
		elif platform.system() == "Windows":
			return "python\\Lib\\site-packages"

	def _getPipList(self, install_dir):
		call = [sys.executable, "-m", "pip", "list", "--path=site-packages"]
		process = subprocess.Popen(call, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		out, err = process.communicate()
		modules = out.decode("utf-8").replace("\r", "")
		call = [sys.executable, "-m", "pip", "list"]
		process = subprocess.Popen(call, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		out, err = process.communicate()
		return modules+"\n"+out.decode("utf-8").replace("\r", "")


	def _installWithPip(self, progress, uninstall_packages, install_packages, url_packages, install_dirs):
		pos = 98-len(install_packages.keys())-len(url_packages.keys())
		if uninstall_packages:
			subprocess.call([sys.executable, "-m", "pip", "uninstall", "-y"]+uninstall_packages)
		if install_packages:
			for package, install_package in install_packages.items():
				progress.setValue(pos, "Installing/Updating {}. Please wait...".format(package.upper()))
				if package in install_dirs:
					subprocess.call([sys.executable, "-m", "pip", "install",  "--target", install_dirs[package], "--upgrade", install_package])
				else:
					subprocess.call([sys.executable, "-m", "pip", "install", "--upgrade", install_package])
				pos += 1
		if url_packages:
			for package, url in url_packages.items():
				filename = os.path.basename(url.replace("https://", ""))
				progress.setValue(pos, "Installing/Updating {}. Please wait...".format(package.upper()))
				if platform.system() == "Darwin":
					subprocess.run(["rm", "-rf", "tmp_pkg"])
					os.mkdir("tmp_pkg")
					os.chdir("tmp_pkg")
					with open(filename, "wb") as f:
						f.write(k.download(url))
					subprocess.run(["unzip", filename])
					os.remove(filename)
					os.chdir(os.listdir(".")[0])
					subprocess.run([sys.executable, "-m", "pip", "install", "-r", "requirements.txt"])
					subprocess.run([sys.executable, "setup.py", "install", "--force"])
					os.chdir("../..")
					subprocess.run(["rm", "-rf", "tmp_pkg"])
				else:
					if package in install_dirs:
						subprocess.call([sys.executable, "-m", "pip", "install",  "--target", install_dirs[package], "--upgrade", url])
					else:
						subprocess.call([sys.executable, "-m", "pip", "install", "--upgrade", url])
				pos += 1
				

	def updatePackages(self, config, kyss_config, progress, install_dir=""):
		if not install_dir:
			install_dir = os.path.abspath(os.getcwd()+os.sep+"..")

		if platform.system() != "Darwin":
			sys.path.append(install_dir+self._pythonPath())

		packages = {}

		if "options.extras_require" in kyss_config and "DIST" in kyss_config["options.extras_require"]:
			for line in kyss_config["options.extras_require"]["DIST"].split("\n"):
				if line and not "platform_system" in line or platform.system() in line:
					sline = re.split("[ ><=]+", line, 3)
					if sline[1] == "@":
						sline[1] = sline[2].split("/")[-1][:-4]+"@"+sline[2]
					packages[sline[0]] = sline[1].replace(";", "")

		if "options.extras_require" in config:
			if "DIST" in config["options.extras_require"]:
				for line in config["options.extras_require"]["DIST"].split("\n"):
					if line and not "platform_system" in line or platform.system() in line:
						sline = re.split("[ ><=]+", line, 3)
						if sline[1] == "@":
							sline[1] = sline[2].split("/")[-1][:-4]+"@"+sline[2]
						packages[sline[0]] = sline[1].replace(";", "")

			if "MODULES" in config["options.extras_require"]:
				for line in config["options.extras_require"]["MODULES"].split("\n"):
					if line and not "platform_system" in line or platform.system() in line:
						sline = re.split("[ ><=]+", line, 3)
						if sline[1] == "@":
							sline[1] = sline[2].split("/")[-1][:-4]+"@"+sline[2]
						packages[sline[0]] = "#"+sline[1].replace(";", "")

		local_packages = {}
		install_dirs = {}
		url_packages = {}
		install_packages = {}
		uninstall_packages = []

		for line in self._getPipList(install_dir).split("\n")[2:]:
			print(line)
			sline = re.split(r"\s+", line)
			if len(sline) >= 2:
				if sline[0].lower() == "proxy_tools":
					sline[0] = "proxy-tools"
				local_packages[sline[0].lower()] = sline[1]

		pos = 50
		progress.setValue(pos, "Checking packages...")
		
		if platform.system() == "Darwin":
			packages = {"pywebview": packages["pywebview"]}
		
		for package, package_version in packages.items():
			uninstall_before = False
			package_url = ""
			force_same_version = False
			if not package in local_packages:
				local_packages[package] = "0"
	
			if package_version[0] == "!":
				package_version = package_version[1:]
				uninstall_before = True

			if package_version[0] == "#":
				package_version = package_version[1:]
				install_dirs[package] = "site-packages"
				uninstall_before = True

			if "@" in package_version:
				force_same_version = True
				package_version, package_url = package_version.split("@", 1)
				if package_version[:4] == "git+":
					package_v = Version(package_url)
					package_url = package_version + "@" + package_url
				else:
					package_v = Version(package_version)
			else:
				package_v = Version(package_version)

			local_v = Version(local_packages[package])
			self.log("\t"+package+" = "+str(local_v)+" VS "+str(package_v))
			if (force_same_version and package_v != local_v) or (package_v > local_v) :
				install_package = package+"=="+package_version

				if uninstall_before:
					uninstall_packages.append(package)
					for dirname in glob.glob("site-packages/"+package+"*"):
						shutil.rmtree(dirname)
			
				if package_url:
					url_packages[package] = package_url
				else:
					install_packages[package] = install_package
		if install_packages or url_packages:
			self._installWithPip(progress, uninstall_packages, install_packages, url_packages, install_dirs)
			#if platform.system() == "Linux":
		#		os.execvp(install_dir+self.APPNAME, [self.APPNAME])


	def updateApp(self, params, progress, install_dir=""):
		VERSION = "-1"
		if k.isfile("VERSION"):
			with open("VERSION") as f:
				VERSION = f.read().strip()

		if VERSION != params["version"]:
			k.log("Updating app {} => {} from {}...".format(VERSION, params["version"], params["download"]))
			zip_update = self.downloadFile(params["download"])
			zip_update = zipfile.ZipFile(BytesIO(zip_update))
			zip_files = zip_update.namelist()
			base_dir = zip_files[0]
			total = len(zip_files)
			processed = 1
			for file in zip_files[1:]:
				filename = file.replace(base_dir, "../")
				basename = os.path.basename(filename)
				if basename and basename[0] == ".":
					continue
				if file[-1] == "/":
					os.makedirs(os.path.dirname(filename), 0o777, True)
				else:
					with zip_update.open(file) as zf, open(filename, "wb") as f:
						shutil.copyfileobj(zf, f)
				progress.setValue(round((90*processed)/total), "Updating File {}...".format(file))
				processed += 1
			zip_update.close()

			with open("VERSION", "w") as f:
				f.write(params["version"])
			
			if k.isdir("../patchs"):
				for patch in glob.glob("../patchs/*.*.py"):
					version = float(os.path.basename(patch[:-3]))
					print(float(VERSION), version)
					if float(VERSION) < version:
						self.log("Apply patch v{}...".format(version))
						subprocess.call([sys.executable, patch])



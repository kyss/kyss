########################################################################
#     ┏┓┏━┓━━━━━━━━━━━━━
#     ┃┃┃┏┛━━━━━━━━━━━━━
#     ┃┗┛┛━┏┓━┏┓┏━━┓┏━━┓
#     ┃┏┓┃━┃┃━┃┃┃━━┫┃━━┫
#     ┃┃┃┗┓┃┗━┛┃┣━━┃┣━━┃
#     ┗┛┗━┛┗━┓┏┛┗━━┛┗━━┛
#     ━━━━━┏━┛┃━━━━━━━━━
#     ━━━━━┗━━┛━━━━━━━━━
#
# This file is part of the Kyss project (https://gitlab.com/ulukyn/kyss).
# Copyright (c) 2020 Nuno Gonçalves (Ulukyn).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import io
import os
import sys
import re
import html
import json
import time
import queue
import string
import locale
import codecs
import appdirs
import urllib3
import certifi
import chevron
import platform
import threading
import traceback
from inspect import getframeinfo, stack
from pathlib import Path, PureWindowsPath, PurePosixPath


from kyss.utils import KyssUtils
from kyss.config import KyssConfig

### Hack to show chevron undefined partials as untranslated sentences
def get_partial(name, partials_dict, module, partials_ext):
	"""Load a partial"""
	if "." in name:
		module, name = name.split(".")

	try:
		# Maybe the partial is in the dictionary
		return partials_dict[module][name]
	except KeyError:
		if KyssModule.app.debug:
			return "{"+module+"."+name+"}"
		return name

k = KyssUtils()


chevron.renderer._get_partial = get_partial

def printMessagesFromQueue():
	k.log("Starting Messages Event Displayer")
	while True:
		scope, module, text, line = KyssModule.messagesQueue.get()
		if scope == "stop":
			break
		elif scope == "error":
			k.error(text, module, line)
		elif scope == "warning":
			k.warning(text, module, line)
		elif scope == "tb":
			k.tb(text, module)
		else:
			k.log(text, module, line)
messageThread = threading.Thread(target=printMessagesFromQueue)
messageThread.start()

class TemplateFormatter(string.Formatter):
	def format_field(self, value, spec):
		if spec.startswith("repeat:"):
			template = spec.partition(":")[-1]
			return ''.join([template.format(name=name, item=item) for name, item in value.items()])
		elif spec == "call":
			return value()
		elif spec.startswith("cycle:"):
			if value % 2:
				return spec.partition(":")[3]
			return spec.partition(":")[2]
		elif spec.startswith("if:"):
			if value:
				return spec.partition(":")[-1]
			return ""
		elif spec.startswith("ifnot:"):
			if not value:
				return spec.partition(":")[-1]
			return ""
		else:
			return super(TemplateFormatter, self).format_field(value, spec)

class KyssModule:
	APPNAME = "Kyss"
	modules = {}
	lang = None
	langs = []
	config = None
	window = None
	windows = {}
	args = None
	js_in_queue = []
	StopAsked = False
	translations = {}
	messagesQueue = queue.Queue()
	i = 0


	def setAppInfos(kyss, name, author, version):
		KyssModule.kyss = kyss
		KyssModule.APPNAME = name
		KyssModule.AUTHOR = author
		KyssModule.VERSION = version

	def _init(self):
		pass

	def __init__(self, module_id=""):
		if module_id:
			self.id = module_id
		else:
			self.id = "_".join(re.findall("[A-Z][^A-Z]*", type(self).__name__)).lower()
		self.setuped = False
		self.k = KyssUtils(self.id)
		self.html_id = self.id
		self.links = {}
		self.tf = TemplateFormatter()
		self.http = urllib3.PoolManager(cert_reqs="CERT_REQUIRED", ca_certs=certifi.where())
		self.getTranslations(self.id)
		self._init()

	def t_cycle(self, text, render):
		result = int(render(text))
		if result % 2:
			return "odd"
		return "even"

	def infos(self):
		pass

### Config Management
	@staticmethod
	def _getConfigPath():
		path = appdirs.user_data_dir(KyssModule.APPNAME, "", roaming=True)
		if not os.path.isdir(path):
			os.makedirs(path)
		return path

	@staticmethod
	def loadConfig(cfg="prefs.cfg"):
		if KyssModule.config == None:
			KyssModule.config = KyssConfig(os.path.join(KyssModule._getConfigPath(), cfg))

	def getConfig(self, section, name):
		return KyssModule.config.get(section, name)

	def setConfig(self, section, name, value):
		KyssModule.config.set(section, name, value)

	def saveConfig(self):
		KyssModule.config.save()

	@staticmethod
	def setApp(app, webview):
		KyssModule.app = app
		KyssModule.webview = webview

	@staticmethod
	def setAndGetLang():
		KyssModule.loadConfig()
		if KyssModule.lang:
			return KyssModule.lang

		KyssModule.lang = "en"
		lang = KyssModule.config.get("app", "lang")
		if lang:
			KyssModule.lang = lang
		else:
			print(KyssModule.langs)
			try: #This code can fail in some unixes or if locale are not correctly set in profile
				KyssModule.lang = locale.getdefaultlocale()[0]
			except:
				if "default" in KyssModule.langs:
					KyssModule.lang = KyssModule.langs["default"]

		if not KyssModule.lang in KyssModule.langs:
			if "default" in KyssModule.langs:
				KyssModule.lang = KyssModule.langs["default"]
	
		return KyssModule.lang


	def addWindow(self, window):
		if not window in KyssModule.windows:
			KyssModule.windows[window] =  self.id
		self.window = window

	def removeWindow(self, window):
		if window in KyssModule.windows:
			KyssModule.windows.remove(window)
		self.window = self.windows.keys()[-1]

	def setup(self):
		self.setuped = True

	def start(self):
		pass

	def stop(self):
		self.StopAsked = True
		self._stop()

	def _stop(self):
		pass

	def call(self, method, args=""):
		func = getattr(KyssModule.modules[self.id], "call_"+method, None)
		if func:
			try:
				if not args:
					func()
				elif type(args) == str:
					func(args)
				else:
					func(*args)
				self.triggerLinks(method.lower(), args)
			except Exception as e:
				self.error(traceback.format_exc())
		else:
			self.warning("no method call_{} in module {} found".format(method, self.id))

	def _(self, name, format_values=()):
		if "." in name:
			module, name = name.split(".")
		else:
			module = self.id

		if module in KyssModule.translations and name in KyssModule.translations[module]:
			if isinstance(format_values, dict):
				try:
					return KyssModule.translations[module][name].format(**format_values)
				except:
					return KyssModule.translations[module][name]
			elif isinstance(format_values, (list, tuple)):
				try:
					return KyssModule.translations[module][name].format(*format_values)
				except:
					return KyssModule.translations[module][name]
			else:
				try:
					return KyssModule.translations[module][name].format(format_values)
				except:
					return KyssModule.translations[module][name]
		else:
			if self.app.debug:
				return "{"+module+"."+name+"}"
			else:
				return name

	def getTranslations(self, module, force=False):
		KyssModule.loadConfig()
		KyssModule.setAndGetLang()
		if force or not module in KyssModule.translations:
			translations = {}
			tr_file = "data/i18n/{}/{}.data".format(KyssModule.lang, module)
			if os.path.isfile(tr_file):
				self.log("Loading i18n ({}) for module {}".format(KyssModule.lang, module))
				with open(tr_file) as f:
					translations = json.loads(f.read())
			KyssModule.translations[module] = translations

		return KyssModule.translations[module]

	def reloadTranslations(self):
		self.getTranslations("app", True)
		for module in KyssModule.translations:
			self.getTranslations(module, True)

	def getTemplateV2(self, name, values={}, partials={}):
		if ":" in name:
			module, name = name.split(":")
		else:
			module = self.id

		if name[0] == "/":
			filename = k.join("data", "html", name[1:]+".html")
		else:
			filename = k.join("modules", module, name+".html")
			if not k.isfile(filename):
				filename = k.join(self.parent_module_path, name+".html")
			
		values["mod_path"] = self.parent_module_path+"/"
		values["mod_name"] = module
		values["id"] = self.id

		if k.isfile(filename):
			with open(filename, "r", encoding="utf-8") as f:
				template = f.read()

				if partials == {}:
					partials = KyssModule.translations

				template = chevron.render(template, values, partials_dict=partials, partials_path=module)
				return template

		return "{} not found".format(filename)

	def templatize(self, dictionary, keys=None):
		final = []
		for name, values in dictionary.items():
			if name == "[ADD]": # Special code to add new stuff
				name = "ADD"
			item = {"_id_": name}
			i = 0
			if type(values) in (tuple, list):
				for value in values:
					if keys:
						if len(keys) > i:
							item[keys[i]] = value
					else:
						item["_v{}_".format(i)] = value
					i += 1
			else:
				item["_value_"] = values
			final.append(item)
		return final

	def esc(self, string):
		return string.replace("\\", "\\\\")

	def posixPath(self, path):
		if platform.system() == "Windows":
			return PureWindowsPath(path).as_posix()
		return path

	def getUsedZones(self):
		return ()

	def link(self, scr_method, dst_module, dst_method):
		if not scr_method in self.links:
			self.links[scr_method] = []
		self.links[scr_method].append([dst_module, dst_method])
		print(self.links)

	def triggerLinks(self, method, args=""):
		print(self.links, method, args)
		if not method in self.links:
			return
		for dst_modules in  self.links[method]:
			module, method = dst_modules
			print(module, method)
			func = getattr(KyssModule.modules[module], "call_"+method, None)
			func()

	def getCssFile(self):
		if self.id == "app":
			return

		css_file = k.path(self.parent_module_path, self.id+".css")
		if os.path.isfile(css_file):
			print("Load css", css_file)
			return os.path.relpath(css_file, os.getcwd())
		
		css_file = k.path(self.parent_module_path, "main.css")
		if os.path.isfile(css_file):
			print("Load css", css_file)
			return os.path.relpath(css_file, os.getcwd())

	def _onDomReady(self):
		for js in KyssModule.js_in_queue:
			self.eval(js)
		KyssModule.js_in_queue = []

	def newThread(self, name, callback, args):
		self.module["app"].threadQueue.put((name, callback, args, True,))

	def newCall(self, name, callback, args):
		self.module["app"].threadQueue.put((name, callback, args, False,))

	def evalFile(self, filename):
		js_file = os.path.abspath(os.path.join("modules", self.id, filename))
		with open(js_file, "r") as f:
			self.eval(f.read())

	def eval(self, js):
		if not KyssModule.windows:
			KyssModule.js_in_queue.append(js)
			return

		if self.StopAsked:
			return

		for window, win_module in KyssModule.windows.items():
			if win_module == self.id or win_module == "app":
				try:
					window.evaluate_js(js)
				except KeyError as e:
					# Window dont exists anymore
					self.windows[window] = None
				except Exception as e:
					self.error(traceback.format_exc())

		# clean windows
		for window in KyssModule.windows.keys():
			if not self.windows[window]:
				del(self.windows[window])

	def getModule(self, name):
		if name in KyssModule.modules:
			return KyssModule.modules[name]
		self.error("Module {} not found".format(name))
		self.log(KyssModule.modules.keys())
		return None

	def getElements(self, search):
		for window in self.windows:
			if window.evaluate_js("document.hasFocus()"):
				return window.get_elements(search)

	def getElementName(self, element):
		if element[0] == "#":
			return self.html_id + element[1:]
		return element

	def getE(self, name, value=""):
		e = self.getElements("#"+self.getElementName(name))
		if e:
			if value:
				if value in e[0]:
					return e[0][value]
				else:
					return ""
			return e[0]
		else:
			self.log("Dom Element not found : {}".format(name))
			return ()

	def setE(self, element, html):
		js = r"""var name = "%s"
		var div = document.getElementById(name)
		if (div != undefined) {
			div.innerHTML = "%s"
		}
		 """ % (self.getElementName(element), html.replace("\"", "\\\"").replace("\n", ""))
		self.eval(js)

	def setupE(self, element, param, value):
		js = r"""var div = document.getElementById("%s")
		if (div != undefined)
			div.%s = %s

		 """ % (self.getElementName(element), param, value)
		self.eval(js)

	def callE(self, element, call):
		js = r"""var div = document.getElementById("%s")
		if (div != undefined)
			div.%s
		else
			console.log("%s is undefined")

		 """ % (self.getElementName(element), call, self.getElementName(element))
		self.eval(js)


	def addToE(self, element, html):
		js = r"""var div = document.getElementById("{}")
		if (div != undefined)
			div.innerHTML += "{}"
		else
			console.log("% is undefined")
		 """.format(self.getElementName(element), html.replace("\"", "\\\"").replace("\n", ""), self.getElementName(element))
		self.eval(js)

	def prependToE(self, element, html):
		js = r"""var div = document.getElementById("%s")
		if (div != undefined) {
			div.innerHTML = "%s" + div.innerHTML
		}
		 """ % (self.getElementName(element), html.replace("\"", "\\\"").replace("\n", ""))
		self.eval(js)

	def showE(self, element):
		js = r"""var div = document.getElementById("%s")
		if (div != undefined) {
			if (div.getAttribute("data-display") != "")
				div.style.display = div.getAttribute("data-display")
			else
				div.style.removeProperty("display")

		}
		 """ % self.getElementName(element)
		self.eval(js)

	def hideE(self, element):
		js = r"""var div = document.getElementById("%s")
		if (div != undefined && div.style.display != "none") {
			div.setAttribute("data-display", div.style.display)
			div.style.display = "none";
		}
		 """ % self.getElementName(element)
		self.eval(js)

	def switchE(self, element):
		js = r"""var div = document.getElementById("%s")
		if (div != undefined) {
			if (div.style.display != "none") {
				div.setAttribute("data-display", div.style.display)
				div.style.display = "none"
			} else {
				if (div.getAttribute("data-display") != "")
					div.style.display = div.getAttribute("data-display")
				else
					div.style.display = "block"
			}
		}
		 """ % self.getElementName(element)
		self.eval(js)

	def showZone(self, zone):
		js = r"""var div = document.getElementById("%s-%s")
		if (div != undefined) {
			if (div.getAttribute("data-display") != "")
				div.style.display = div.getAttribute("data-display")
			else
				div.style.display = "block"

		}
		 """ % (zone, self.id)
		self.eval(js)

	def hideZone(self, zone):
		js = r"""var div = document.getElementById("%s-%s")
		if (div != undefined && div.style.display != "none") {
			div.setAttribute("data-display", div.style.display)
			div.style.display = "none"
		}
		 """ % (zone, self.id)
		self.eval(js)

	def removeZone(self, zone):
		js = r"""var div = document.getElementById("%s")
		if (div != undefined) {
			div.parentNode.removeChild(div);
		}
		 """ % zone
		self.eval(js)

	def setZone(self, zone, html):
		if zone != "popup-content":
			zone += "-" + self.id
		js = r"""var div = document.getElementById("%s")
		if (div != undefined) {
			div.innerHTML = "%s"
		}
		 """ % (zone, html.replace("\"", "\\\"").replace("\n", ""))
		self.eval(js)

	def addToZone(self, zone, html):
		if not html:
			return
		js = r"""var div = document.getElementById("%s-%s")
		if (div != undefined) {
			div.innerHTML += "%s"
		}
		 """ % (zone, self.id, html.replace("\"", "\\\"").replace("\n", ""))
		self.eval(js)

	def cleanZones(self, zone):
		js = r"""var elements = document.querySelectorAll('[id^=%s-]');
		elements.forEach(function(e) {
			e.innerHTML = ""
		});
		 """ % zone
		self.eval(js)


	def htmlEncode(self, text):
		return html.escape(text)

	def call_ClosePopup(self):
		self.hideE("popup")

	def openFileDialog(self, file_types, allow_multiple=False):
		return self.window.create_file_dialog(self.webview.OPEN_DIALOG, allow_multiple=allow_multiple, file_types=file_types)

	def openFolderDialog(self):
		return self.window.create_file_dialog(self.webview.FOLDER_DIALOG)

	def downloadFile(self, url, method="GET", callback=None, headers={}):
		# Todo manage fails
		try:
			response = self.http.request(method, url, preload_content=False, headers=headers)
		except:
			self.error("Max retry error: {}".format(url))
			return None

		if not response.status == 200:
			self.error("Downloading {}".format(url))
			return None
		if callback:
			final = b""
			while True:
				data = response.read(131072)
				if not data:
					break
				callback(len(data))
				final += data
			response.release_conn()
			return final
		else:
			return response.read()

	def color(self, name):
		self.k.c(name)

	def log(self, text):
		KyssModule.messagesQueue.put(("log", self.id, text, getframeinfo(stack()[1][0]).lineno))

	def error(self, text):
		KyssModule.messagesQueue.put(("error", self.id, text, getframeinfo(stack()[1][0]).lineno))

	def warning(self, text):
		KyssModule.messagesQueue.put(("warning", self.id, text, getframeinfo(stack()[1][0]).lineno))

	def tb(self):
		KyssModule.messagesQueue.put(("tb", self.id, "\n"+"".join(traceback.format_stack())))

########################################################################
#     ┏┓┏━┓━━━━━━━━━━━━━
#     ┃┃┃┏┛━━━━━━━━━━━━━
#     ┃┗┛┛━┏┓━┏┓┏━━┓┏━━┓
#     ┃┏┓┃━┃┃━┃┃┃━━┫┃━━┫
#     ┃┃┃┗┓┃┗━┛┃┣━━┃┣━━┃
#     ┗┛┗━┛┗━┓┏┛┗━━┛┗━━┛
#     ━━━━━┏━┛┃━━━━━━━━━
#     ━━━━━┗━━┛━━━━━━━━━
#
# This file is part of the Kyss project (https://gitlab.com/ulukyn/kyss).
# Copyright (c) 2021 Nuno Gonçalves (Ulukyn).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import abc
import json
import venv
import glob
import shutil
import urllib3
import certifi
import chevron
import zipfile
import platform
import subprocess
import sysconfig
import configparser
from io import BytesIO

from kyss.utils import KyssUtils, abstractmethod
k = KyssUtils("KyssCli")
config = configparser.ConfigParser()
kyss_dir = os.path.dirname(__file__)

http = urllib3.PoolManager(cert_reqs="CERT_REQUIRED",  ca_certs=certifi.where())
def checkSetup(create=False):

	if not k.isfile("setup.cfg"):
		if create:
			if k.isfile(kyss_dir+"/dev/default_setup.cfg"):
				with open(kyss_dir+"/dev/default_setup.cfg") as f:
					template = f.read()
				name = input("Enter the app name: ") 
				desc = input("Enter the app description: ") 
				author = input("Enter your name: ") 
				email = input("Enter your email: ") 
				gitlab = input("Enter gitlab group (empty if not using gitlab): ") 
				#gitlab = input("Are you using gitlab or github (gitlab only for now)?")
				template = chevron.render(template, {"name": name, "desc": desc, "author": author, "email": email, "gitlab": gitlab})
				with open("setup.cfg", "w") as f:
					f.write(template)
		else:
			k.perror("Error: 'setup.cfg' not found. Please install or create Kyss APP")
			sys.exit(0)
	
	try:
		config.read("setup.cfg")
	except Exception as e:
		k.perror("Error: 'setup.cfg' have errors:")
		print(e)
		sys.exit(0)

def main():
	options = [cliInstall(), cliRun(), cliDebug(), cliPackages(), cliModules(), cliEnv(), cliBuild(), cliVersion(), cliExit()]
	kyss_dir = os.path.dirname(__file__)

	print(k.bg("green")+k.color("black")+"\n")
	print(k.style("bold")+" "*10+"🔱 Welcome to Kyss 🔱")
	print(k.style("normal"))

	args = sys.argv
	interactive = False
	while True:
		if len(args) > 1:
			correct_arg = False
			for i, option in enumerate(options):
				if args[1] == option.getName()[0] or args[1] == option.getName()[1]:
					correct_arg = True
					if not option.getOptions():
						if len(args) > 3:
							option.execute(args[2], args[3:])
						elif len(args) > 2:
							option.execute(args[2])
						else:
							option.execute()
						args = [args[0]]
						break
					elif len(args) > 2:
						option.execute(args[2], args[3:])
						if not interactive:
							sys.exit(0)

						if args[2] == "q":
							sys.exit(0)
						elif args[2] == "b":
							args = [args[0]]
						else:
							args = [args[0], args[1]]
						break
					else:
						k.pe("yellow", "   kyss "+option.getName()[1])
						printOptions(option.getOptions())
						arg = getInput("Select option: ")
						args.append(arg)
						interactive = True

			if not correct_arg:
				k.perror("Wrong option!")
				args = [args[0]]
		else:
			actions_options = []
			for i, option_name in enumerate(options):
				actions_options.append(option_name.getName())
			printOptions(actions_options)
			arg = getInput("Select option: ")
			args.append(arg)
			interactive = True

def printOptions(options):
	print(k.bg("yellow")+"\n"+k.style("normal"))
	for i in range(len(options)):
		option_name = options[i]
		print(k.color("white", "bright")+option_name[0]+") "+option_name[1], end="")
		print(k.color("white", "bright")+k.style("normal")+" "*(12-len(option_name[0]+option_name[1]))+" "+option_name[2], end="")
		if i < len(options)-1:
			print()
	print(k.bg("yellow")+"\n"+k.style("normal"))

def getInput(text, color="normal"):
	k.pe(color, text)
	with k.term.cbreak():
		key = k.term.inkey()
		k.p(color, key)
		return key

def getPython():
	if k.isdir("env"):
		builder = venv.EnvBuilder()
		context = builder.ensure_directories("env")
		return context.env_exe
	else:
		return sys.executable

########################################################################
class cliAction(object):

	def __init__(self, config=None):
		self.config = config

	@abstractmethod
	def getName(self):
		return ""

	@abstractmethod
	def execute(self, arg="", args=[]):
		pass

	def getOptions(self):
		return []


########################################################################
class cliRun(cliAction):

	def getName(self):
		return ("r", "run", "Manage start of Kyss App")

	def createStarterScript(self):
		appname = config["metadata"]["name"]
		with open(appname+".py", "w") as f:
			f.write("""
import os
import re
import sys
import urllib3
import certifi
import platform
import subprocess
import configparser

http = urllib3.PoolManager(cert_reqs="CERT_REQUIRED",  ca_certs=certifi.where())

local_config = configparser.ConfigParser()
local_config.read("setup.cfg")
remote_config = local_config

url = local_config["metadata"]["setup"]
try:
	response = http.request("GET", url, preload_content=False)
except:
	print("Error downloading A {}".format(url))
else:
	if not response.status == 200:
		print("Error Downloading {}".format(url))
	else:
		data = response.read().decode()
		response.release_conn()
		remote_config = configparser.ConfigParser()
		remote_config.read_string(data)

local_version = float(local_config["metadata"]["version"])
remote_version = float(remote_config["metadata"]["version"])

app_name = local_config["metadata"]["name"]
app_name = app_name[0].upper()+app_name[1:]
print(app_name, "version : {} vs {}".format(local_version, remote_version))
if local_version < remote_version:
	with open("setup.cfg", "w") as f:
		f.write(data)
	config = remote_config
else:
	config = local_config

kyss_setup = config["metadata"]["kyss"]
if kyss_setup[:18] == "https://gitlab.com":
	kyss_setup_url = kyss_setup.replace("/archive/", "/raw/").replace(kyss_setup.split("/")[8], "setup.cfg")
elif kyss_setup[:18] == "https://github.com":
	kyss_setup_url = kyss_setup.replace("/archive/refs/tags/", "/raw/").replace(".zip", "/setup.cfg")

remote_version = 0

try:
	response = http.request("GET", kyss_setup_url, preload_content=False)
except:
	print("Error downloading A {}".format(url))
else:
	data = response.read().decode()
	response.release_conn()
	kyss_config = configparser.ConfigParser()
	kyss_config.read_string(data)
	remote_version = float(kyss_config["metadata"]["version"])

call = [sys.executable, "-m", "pip", "list"]
process = subprocess.Popen(call, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
out, err = process.communicate()
modules = out.decode("utf-8").replace("\\r", "")
local_version = -1
for line in modules.split("\\n")[2:]:
	sline = re.split(r"\\s+", line)
	if len(sline) >= 2:
		if sline[0].lower() == "kyss":
			local_version = float(".".join(sline[1].split(".")[:2]))

print("Kyss version : {} vs {}".format(local_version, remote_version))
if local_version < remote_version:
	if platform.system() == "Darwin":
		subprocess.run(["curl", kyss_setup, "--output", "kyss.zip"])
		subprocess.run(["rm", "-rf", "kyss-main"])
		subprocess.run(["unzip", "kyss.zip"])
		os.chdir("kyss-main")
		os.remove("setup.cfg")
		subprocess.run([sys.executable, "setup.py", "install", "--force"])
		os.chdir("..")
		subprocess.run(["rm", "-rf", "kyss-main"])
	else:
		subprocess.run([sys.executable, "-m", "pip", "uninstall", "-y", "kyss"])
		subprocess.run([sys.executable, "-m", "pip", "install", "--upgrade", kyss_setup])


os.chdir("app")
sys.path.append(os.getcwd())

from kyss import kyss
kysspy = kyss.Kyss(config, kyss_config)
kysspy.start()
""")

	def execute(self, arg="", args=[]):
		checkSetup()
		script = config["metadata"]["name"]+".py"
		if not k.isfile(script):
			self.createStarterScript()
		builder = venv.EnvBuilder()
		context = builder.ensure_directories("env")
		if not k.isfile(context.env_exe):
			k.perror("Python in virtual env not found....")
			env = cliEnv()
			env.create()
			env.kyss()

		cmd = [context.env_exe, script]+sys.argv[2:]
		k.pexecute(cmd)
		sys.exit(0)

########################################################################
class cliDebug(cliAction):
	
	def getName(self):
		return ("d", "debug", "Start the Kyss App in debug mode")

	def execute(self, arg="", args=[]):
		checkSetup()
		script = config["metadata"]["name"]
		builder = venv.EnvBuilder()
		context = builder.ensure_directories("env")
		if not k.isfile(context.env_exe):
			k.perror("Python in virtual env not found....")
			env = cliEnv()
			env.create()
			env.kyss()

		cmd = [context.env_exe, script+".py", "-d 1"]+sys.argv[2:]
		k.pexecute(cmd)
		sys.exit(0)


########################################################################
class cliInstall(cliAction):

	def getName(self):
		return ("i", "install", "Create/Install a Kyss App")

	def getOptions(self):
		return [
			("n", "new", "Create a new Kyss App"),
			("i", "install", "Install a Kyss App"),
			("b", "back", "Back to main menu"),
			("q", "quit", "Quit")]

	def create(self):
		name = config["metadata"]["name"]
		kyss_dir = os.path.dirname(__file__)
		if not k.isfile(name+".py"):
			shutil.copy(kyss_dir+"/dev/starter.py", name+".py")
		
		if not k.isdir("app"):
			shutil.copytree(kyss_dir+"/dev/app", "app")
		
		if not k.isdir("env"):
			env = cliEnv()
			env.create()
			env.kyss()
	
		k.pdone("Creation finished")
		sys.exit(0)

	def execute(self, arg="", args=[]):
		checkSetup(True)
		
		if arg == "n" or arg == "new":
			self.create()
		elif arg == "i" or arg == "install":
			k.error("todo")


########################################################################
class cliEnv(cliAction):

	def getName(self):
		if not k.isdir("env"):
			return ("e", "env", "Create the Python Virtual Env")
		return ("e", "env", "Manage python virtual env")

	def getOptions(self):
		if not k.isdir("env"):
			return []
		return [
			("r", "remove", "Remove the Python Virtual Env"),
			("l", "list", "List packages"),
			("i", "install", "Install a  package"),
			("k", "kyss", "Install kyss in editable mode"),
			("p", "pywebview", "Install pywebview in editable mode"),
			("d", "dev", "Install modules in editable mode"),
			("s", "shell", "Run interactive shell"),
			("b", "back", "Back to main menu"),
			("q", "quit", "Quit")]
			
	def execute(self, arg="", args=[]):
		if not k.isdir("env"):
			self.create()
			return

		if arg == "r" or arg == "remove":
			self.create()
		elif arg == "l" or arg == "list":
			self.getList()
		elif arg == "i" or arg == "install":
			self.install(args)
		elif arg == "k" or arg == "kyss":
			self.kyss()
		elif arg == "p" or arg == "pywebview":
			self.pywebview()
		elif arg == "d" or arg == "dev":
			self.dev()
		elif arg == "s" or arg == "shell":
			self.shell()
		elif arg == "q" or arg == "quit":
			return
		else:
			k.perror("Unknow option: {}".format(arg))
			sys.exit(1)

	def getList(self, arg="", args=[]):
		cmd = [getPython(), "-m", "pip", "list"]
		k.pexecute(cmd)
		input("Please press enter to back to menu...")

	def dev(self, arg="", arges=[]):
		if k.isdir("modules"):
			for module in os.listdir("modules"):
				with open("modules/"+module+"/pyproject.toml", "r") as f:
					lines = f.read().split("\n")
				for line in lines:
					if line[:7] == "name = ":
						module_name = line[7:].replace("\"", "")
						for dirname in glob.glob("app/site-packages/"+module_name+"*"):
							shutil.rmtree(dirname)

				builder = venv.EnvBuilder()
				context = builder.ensure_directories("env")
				cmd = [context.bin_path+"/python", "-m", "pip", "install", "-U", "-e", "modules/"+module]
				k.pexecute(cmd)
			k.pdone()

	def kyss(self, arg="", args=[]):
		venv.main(("env", ))
		builder = venv.EnvBuilder()
		context = builder.ensure_directories("env")
		cmd = [context.bin_path+"/pip", "install", "-U", "pip"]
		k.execute(cmd)
		if k.isdir("../kyss"):
			kyss_path = "../kyss"
		else:
			kyss_path = input("Please enter path to your Kyss clone:")
		python_exe = context.bin_path+"/python"
		cmd = [python_exe, "-m", "pip", "uninstall",  "kyss"]
		k.pexecute(cmd)
		cmd = [python_exe, "-m", "pip", "install", "-e", kyss_path]
		k.pexecute(cmd)

	def create(self, arg="", args=[]):
		if k.isdir("env"):
			k.pexec("Removing Python Virtual Env")
			shutil.rmtree("env")
			k.pdone()
			return
		k.pexec("Creating Python Virtual Env")
		venv.main(("env", ))
		builder = venv.EnvBuilder()
		context = builder.ensure_directories("env")
		cmd = [context.bin_path+"/pip", "install", "-U", "pip"]
		k.execute(cmd)

		k.pdone()
		k.pexec("Upgrading Pip")
		cmd = [context.env_exe, "-m", "pip", "install", "--upgrade", "pip"]
		k.execute(cmd)
		k.pdone()
		
		kyss_install = getInput("Do you want install Kyss in editable mode (y/n)?")
		if kyss_install == "y":
			self.kyss()
		else:
			kyss_config = configparser.ConfigParser()
			kyss_config.read(kyss_dir+"/../setup.cfg")
			cmd = [context.env_exe, "-m", "pip", "install", kyss_config["metadata"]["download"]]
			k.pexecute(cmd)
		k.pdone()
	
	def none(self):
		k.pexec("Installing Requirements, Please wait")
		with open("requirements.txt", "w") as f:
			f.write(config["options"]["install_requires"])
		cmd = [context.env_exe, "-m", "pip", "install", "-v", "-r", "requirements.txt"]
		k.execute(cmd)
		k.pdone()
		
		k.pexec("Installing Dist Requirements, Please wait")
		with open("requirements.txt", "w") as f:
			f.write(config["options.extras_require"]["DIST"])
		cmd = [context.env_exe, "-m", "pip", "install", "-v", "-r", "requirements.txt"]
		k.execute(cmd)
		k.pdone()
		os.unlink("requirements.txt")

	def shell(self, arg="", args=[]):
		cmd = ["xterm", "-hold", "-e", "bash -i <<< 'source env/bin/activate; exec </dev/tty'"]
		k.pexecute(cmd)


########################################################################
class cliModules(cliAction):

	def getName(self):
		return ("m", "modules", "Display Modules options")

	def execute(self, arg="", args=[]):
		if arg == "l" or arg == "list":
			self.getList()
		elif arg == "i" or arg == "install":
			self.install()
		elif arg == "f" or arg == "fix":
			self.installAll()
		elif arg == "q" or arg == "quit":
			sys.exit(0)

	def getOptions(self):
		options = [
			("l", "list", "List modules"),
			("i", "install", "Install a module"),
			("f", "fix", "Install all required modules"),
			("b", "back", "Back to main menu"),
			("q", "quit", "Quit")]
			
		return options

	def _getList(self):
		used_modules = {}
		config = configparser.ConfigParser()
		config.read("app/data/modules.ini")
		for section in config.sections():
			if section == "menu":
				continue
			for name, value in config[section].items():
				if value:
					name = value
				used_modules[name] = 1
		for module in os.listdir("app/modules"):
			if module == "__init__.py":
				continue
			if not module in used_modules:
				used_modules[module] = 0
			else:
				used_modules[module] = 2
		return used_modules
	
	def getList(self):
		used_modules = self._getList()
		status = (k.color("pink")+"unused", k.color("red")+"not installed", k.color("green")+"installed and used")
		for module, value in used_modules.items():
			k.pinfo("{}: {}".format(k.color("info")+module, status[value]))

	def installAll(self):
		used_modules = self._getList()
		for module, value in used_modules.items():
			if value == 1:
				k.ptitle("Installing: {}".format(module))
				found = ""
				for module_repos in os.listdir("modules/"):
					if k.isdir("modules/"+module_repos+"/"+module):
						k.p("green", "Found repository!")
						found = module_repos
				
				if found == "":
					k.pwarning("Repository not found in modules/")
					git_url = input("Please enter the git url:")
					found = git_url.split("/")[-1][:-4]
					k.pexecute(["git", "clone", git_url, "modules/"+found])
				k.pinfo("Run as root: ")
				k.p("yellow", " ".join(["mount", "--bind", os.path.abspath("modules/"+found+"/"+module), "app/modules/"+module]))
				os.makedirs("app/modules/"+module, 0o777, True)
				k.pexecute(["sudo", "mount", "--bind", os.path.abspath("modules/"+found+"/"+module), "app/modules/"+module])
				k.pdone("Installed")



########################################################################
class cliPackages(cliAction):

	def getName(self):
		return ("p", "packages", "Display Packages options (WIP)")

	def execute(self, arg="", args=[]):
		if arg == "l" or arg == "list":
			self.getList()
		elif arg == "a" or arg == "add":
			self.add()
		elif arg == "i" or arg == "install":
			self.install(args)
		elif arg == "q" or arg == "quit":
			sys.exit(0)

	def getOptions(self):
		options = [
			("l", "list", "List packages"),
			("i", "install", "Install a Python Package into Kyss App"),
			("a", "add", "Add a Kyss Package"),
			("h", "help", "Display the help"),
			("b", "back", "Back to main menu"),
			("q", "quit", "Quit")]
		return options


	def getPipList(self):
		python = getPython()
		cmd = [getPython(), "-m", "pip", "list", "--format=json"]
		(stdout, stderr) = k.execute(cmd)
		packages = json.loads(stdout)
		final = {}
		for package in packages:
			final[package["name"]] = package["version"]
		return final

	def add(self):
		name = input("Please enter the name of module:")
		url = input("Please enter the url of module.ini file:")
		config = configparser.ConfigParser()
		config.read("app.ini")
		config["MODULES"][name] = url
		with open("app.ini", "w") as configfile:
			config.write(configfile)
		gitClone(name, url)

	def clone(self, name, url):
		k.pexec("Cloning module {}".format(name))
		cmd = ["git", "clone", url.split("/-/")[0], k.path("modules/"+name)]
		k.execute(cmd)


	def install(self, args, dev=False):
		installed_packages = self.getPipList()
		if not args:
			k.ptitle("Installation of module")
			have_appini = k.isfile("app.ini")
			if have_appini:
				k.pdone("app.ini found, installation with names available")
			print()
			print("Please enter one or more modules separated by spaces:")
			if have_appini:
				k.pinfo("   using name if declared in app.ini (separated by space)")
			k.pe("yellow", "or")
			k.pinfo(" using url to the module.ini file of a kyss module")
			k.pe("yellow", "or")
			k.pinfo(" using location to a kyss module folder with module.ini")
		else:
			for arg in args:
				if arg[0:4] == "http":
					config = configparser.ConfigParser()
					module_ini = k.download(arg)
					if module_ini:
						try:
							config.read_string(module_ini.decode())
							version = config["module"]["version"]
							name = config["module"]["name"]
							package = config["module"]["download"]
						except Exception as e:
							k.perror("Invalid module.ini : {}".format(e))
							continue

						k.pcheck("Checking installation of {}".format(k.color("info")+name+k.color("check")))

						if not name in installed_packages or installed_packages[name] != version:
							if installed_packages[name] != version:
								k.pwarning("Package version missmatch : {} vs {}".format(installed_packages[name], version))
							else:
								k.pwarning("Package not installed")
							self.installPackageWithUrl(package, dev)
						else:
							k.pdone("Package installed and up to date")


	def installPackageWithUrl(self, url, dev=False):
		k.pexec("Installing module {}".format(url))
		if dev:
			cmd = [k.path(getPython()), "-m", "pip", "-e", "install", url, "--no-warn-script-location"]
		else:
			cmd = [k.path(getPython()), "-m", "pip", "install", url, "--no-warn-script-location"]
		k.execute(cmd)

	def installPackageInFolder(self, url, dev=False):
		k.pexec("Installing module {}".format(name))
		cwd = os.getcwd()
		os.chdir(k.path("modules/"+name))
		builder = venv.EnvBuilder()
		context = builder.ensure_directories("env")
		cmd = [context.bin_path+"/pip", "install", ".", "--no-warn-script-location"]
		k.execute(cmd)
		os.chdir(cwd)


class cliBuild(cliAction):

	def getName(self):
		return ("b", "build", "Show Build Packages options")

	def execute(self, arg="", args=[]):
		if not k.isdir("env"):
			k.perror("Please create env before..")
			sys.exit(1)
			
		if not k.isdir("kyss-dev"):
			dist = k.download("https://gitlab.com/ulukyn/kyss-dev/-/archive/main/kyss-dev-main.zip")
			filebytes = BytesIO(dist)
			myzipfile = zipfile.ZipFile(filebytes)
			myzipfile.extractall(".")
			os.rename("kyss-dev-main", "kyss-dev")

		if arg == "w" or arg == "windows":
			cmd = ["xterm", "-hold", "-e", "bash "+os.getcwd()+"/kyss-dev/windows/build.sh 64"]
		elif arg == "i" or arg == "windows32":
			cmd = ["xterm", "-hold", "-e", "bash "+os.getcwd()+"/kyss-dev/windows/build.sh 32"]
		elif arg == "l" or arg == "linux":
			cmd = ["xterm", "-hold", "-e", "bash "+os.getcwd()+"/kyss-dev/linux/build.sh"]
		elif arg == "q" or arg == "quit":
			return
		else:
			k.perror("Unknow option: {}".format(arg))
			sys.exit(1)
		k.execute(cmd)

	def getOptions(self):
		if not k.isdir("env"):
			return []
		return [
			("i", "windows32", "Build Windows 32 package"),
			("w", "windows", "Build Windows 64 package"),
			("l", "linux", "Build Linux package"),
			("b", "back", "Back to main menu"),
			("q", "quit", "Quit")]


class cliVersion(cliAction):

	def getName(self):
		return ("v", "version", "Update the version")

	def execute(self, arg="", args=[]):
		if not arg:
			arg = "0.1"
			if k.isfile("app/VERSION"):
				with open("app/VERSION", "r") as f:
					arg ="{:.1f}".format(float(f.read().strip())+0.1)

		if k.isdir("VERSION"):
			with open("app/VERSION", "w") as f:
				f.write(arg)
	
		for file in ["pyproject.toml", "setup.cfg", "setup.ini"]:
			if k.isfile(file):
				with open(file, "r") as f:
					lines = f.read().split("\n")
				final = []
				for line in lines:
					if line[:11] == "version = \"":
						line = "version = \"{}\"".format(arg)
					elif line[:10] == "version = ":
						line = "version = {}".format(arg)
					final.append(line)
				with open(file, "w") as f:
					f.write("\n".join(final))
		sys.exit(0)


class cliExit(cliAction):

	def getName(self):
		return ("q", "quit", "Quit kyss")

	def execute(self, arg="", args=[]):
		k.p("yellow", "Goodbye!")
		sys.exit(0)


#!/bin/sh

# Reset
ColorOff='\e[0m'       # Text Reset

# Regular Colors
Black='\e[0;30m'        # Black
Red='\e[0;31m'          # Red
Green='\e[0;32m'        # Green
Yellow='\e[0;33m'       # Yellow
Blue='\e[0;34m'         # Blue
Purple='\e[0;35m'       # Purple
Cyan='\e[0;36m'         # Cyan
White='\e[0;37m'        # White

# Bold
BBlack='\e[1;30m'       # Black
BRed='\e[1;31m'         # Red
BGreen='\e[1;32m'       # Green
BYellow='\e[1;33m'      # Yellow
BBlue='\e[1;34m'        # Blue
BPurple='\e[1;35m'      # Purple
BCyan='\e[1;36m'        # Cyan
BWhite='\e[1;37m'       # White

# Underline
UBlack='\e[4;30m'       # Black
URed='\e[4;31m'         # Red
UGreen='\e[4;32m'       # Green
UYellow='\e[4;33m'      # Yellow
UBlue='\e[4;34m'        # Blue
UPurple='\e[4;35m'      # Purple
UCyan='\e[4;36m'        # Cyan
UWhite='\e[4;37m'       # White

# Background
OnBlack='\e[40m\e[K'       # Black
OnRed='\e[41m\e[K'         # Red
OnGreen='\e[42m\e[K'       # Green
OnYellow='\e[43m\e[K'      # Yellow
OnBlue='\e[44m\e[K'        # Blue
OnPurple='\e[45m\e[K'      # Purple
OnCyan='\e[46m\e[K'        # Cyan
OnWhite='\e[47m\e[K'       # White

# High Intensity
IBlack='\e[0;90m'       # Black
IRed='\e[0;91m'         # Red
IGreen='\e[0;92m'       # Green
IYellow='\e[0;93m'      # Yellow
IBlue='\e[0;94m'        # Blue
IPurple='\e[0;95m'      # Purple
ICyan='\e[0;96m'        # Cyan
IWhite='\e[0;97m'       # White

# Bold High Intensity
BIBlack='\e[1;90m'      # Black
BIRed='\e[1;91m'        # Red
BIGreen='\e[1;92m'      # Green
BIYellow='\e[1;93m'     # Yellow
BIBlue='\e[1;94m'       # Blue
BIPurple='\e[1;95m'     # Purple
BICyan='\e[1;96m'       # Cyan
BIWhite='\e[1;97m'      # White

# High Intensity backgrounds
OnIBlack='\e[0;100m\e[K'   # Black
OnIRed='\e[0;101m\e[K'     # Red
OnIGreen='\e[0;102m\e[K'   # Green
OnIYellow='\e[0;103m\e[K'  # Yellow
OnIBlue='\e[0;104m\e[K'    # Blue
OnIPurple='\e[0;105m\e[K'  # Purple
OnICyan='\e[0;106m\e[K'    # Cyan
OnIWhite='\e[0;107m\e[K'   # White

function c {
	printf "$*${ColorOff}\n"
}
function cn {
	printf "$*${ColorOff}"
}

function display_center() {
	columns="$(tput cols)"
	line=$*
	printf "%*s\n" $(((${#line} + columns)/2)) "$line"
}

function header() {
	c $Black$OnGreen
	c $Black$OnGreen "$(display_center "-= Welcome to Kyss =-")"
	c $Black$OnGreen
}

function getConfig() {
	if [ -f ~/.config/ryztart.prefs ]
	then
		. ~/.config/ryztart.prefs
	else
		mkdir -p ~/.config/
		touch ~/.config/ryztart.prefs
	fi
}

function getVar() {
	VAR=${!1}
	if [ -z "$VAR" ]
	then
		read -p "Enter a value for $1:" VAR
		echo "$1=\"$VAR\"" >> ~/.config/ryztart.prefs
	fi
	
	echo $VAR
}


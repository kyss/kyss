########################################################################
#     ┏┓┏━┓━━━━━━━━━━━━━
#     ┃┃┃┏┛━━━━━━━━━━━━━
#     ┃┗┛┛━┏┓━┏┓┏━━┓┏━━┓
#     ┃┏┓┃━┃┃━┃┃┃━━┫┃━━┫
#     ┃┃┃┗┓┃┗━┛┃┣━━┃┣━━┃
#     ┗┛┗━┛┗━┓┏┛┗━━┛┗━━┛
#     ━━━━━┏━┛┃━━━━━━━━━
#     ━━━━━┗━━┛━━━━━━━━━
#     
# This file is part of the Kyss project (https://gitlab.com/ulukyn/kyss).
# Copyright (c) 2020 Nuno Gonçalves (Ulukyn).
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
 
from kyss.module import KyssModule

class KyssMenu(KyssModule):
	
	def getUsedZones(self):
		self.selectedMenu = ""
		return {"left": 100}
	
	def call_SelectMenu(self, module):
		if self.selectedMenu:
			self.callE("menu-"+self.selectedMenu, "classList.remove(\"selected\")")
		self.selectedMenu = module
		self.callE("menu-"+self.selectedMenu, "classList.add(\"selected\")")
		
	def call_OpenMenu(self):
		self.showE("menu")

	def call_SwitchMenu(self):
		self.switchE("menu")
	
	def setup(self):
		html= ""
		if "menu" in self.app.modules_config:
			for name, value in self.app.modules_config["menu"].items():
				svalue = value.split(" ")
				menu_config = {}
				menu_config[svalue[0]+"?"] = True # Enable the menu button type
				menu_config["name"] = name
				menu_config["tooltip"] = self._(name)
				if len(svalue) > 1 :~
					menu_config["action"] = svalue[1]
				if len(svalue) > 2 :
					menu_config["icon"] = svalue[2]
				html += self.getTemplateV2("menu_button", menu_config)
		
		
		values = {"content": html}
		html = self.getTemplateV2("kyss_menu", values)
		self.setZone("left", html)
		KyssModule.setup(self)
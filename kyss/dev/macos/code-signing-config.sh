# Configure code signing.

# To enable signing, set this to "1".  Otherwise, no signing is done; useful
# for local testing.
DO_CODE_SIGNING=0

SIGNING_IDENTITY="Goncalves Nuno"
# The package signing identity corresponds to a "3rd Party Mac Developer Application"
# certificate that resides within the Keychain Access application.
SIGNING_IDENTITY_APP="Developer ID Application: Goncalves Nuno"

# The package signing identity corresponds to a "3rd Party Mac Developer Installer"
# certificate that resides within the Keychain Access application.
SIGNING_IDENTITY_INSTALLER="Developer ID Installer: Goncalves Nuno"
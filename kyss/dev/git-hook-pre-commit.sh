#!/bin/sh

REVISION=$(git rev-list HEAD --count)
sed -i "s/version_minor = .*/version_minor = $REVISION/" app/data/app.ini
git add app/data/app.ini

find . -name ".directory" -exec rm -rf {} \;
find . -name "__pycache__" -exec rm -rf {} \;

mv files.info files.info.last
mv files.sha1 files.sha1.last

BRANCH=$(git branch --show-current)

cd app
rm -rf .cache/
rm -f *.log
FILES=$(find . | sort)
for file in $FILES
do
	if [[ -f  $file ]]
	then
		CHECK=$(git status -s $file)
		if [[ ${CHECK:0:2} == " M" ]] || [[ ${CHECK:0:2} == "A " ]]
		then
			grep $file ../files.info.last >> ../files.info
			grep $file ../files.sha1.last >> ../files.sha1
		elif [[ ${CHECK:0:2} != "??" ]]
		then
			#echo "$file - ${CHECK:0:2}" 
			stat --format="%n %s %Y" $file >> ../files.info
			sha1sum $file >> ../files.sha1
		fi
	fi
done


git status -s | while read -r CHECK
do
	echo "${CHECK:2} - ${CHECK:0:2}" 
	if [[ ${CHECK:0:2} == "D " ]]
	then
		echo ${CHECK:2} >> ../files.rm
	fi
done

cd ..

sed -i "s/^\.\///g" files.info 
sed -i "s/ \.\///g" files.sha1

rm files.info.last
rm files.sha1.last

git add files.info
git add files.sha1
git add files.rm



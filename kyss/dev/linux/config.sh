#!/bin/bash

PYTHON_VERS="38"
PYTHON_VERSION="3.8"
PYTHON_FULL_VERSION="3.8.14"

capitalize_first()
{
	string0="$@"
	firstchar=${string0:0:1}
	string1=${string0:1}
	FirstChar=`echo "$firstchar" | tr a-z A-Z`
	echo "$FirstChar$string1"
}

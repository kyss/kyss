;--------------------------------
;Include Modern UI
!include "MUI2.nsh"

;--------------------------------
;General
!define Company "Ryzom"
!define Product "${Executable}"
!define RegistryCat "HKCU"
!define RegistryKey "Software\${Company}\${GenericProduct}"
!define RegistryWindowsUninstall "Software\Microsoft\Windows\CurrentVersion\Uninstall"
!define SrcDir "APP"
!define Installer "${Executable}Installer.exe"

;Properly display all languages (Installer will not work on Windows 95, 98 or ME!)
;Unicode true

;New XP style
XPStyle on

;Name and file
Name "${Product}"
OutFile "${Installer}"

;Default installation folder
InstallDir "$LOCALAPPDATA\${GenericProduct}"

;Get installation folder from registry if available
InstallDirRegKey "${RegistryCat}" "${RegistryKey}" "${GenericProduct} Install Path"

;Request application privileges for Windows Vista
RequestExecutionLevel admin

;Best compression
SetCompressor LZMA

; ???
AllowSkipFiles on

Var DirectXSetupError
Var VCRedistSetupError

;--------------------------------
;Interface Settings

!define MUI_ICON "..\app\data\${Product}.ico"
!define MUI_UNICON "..\app\data\Uninstall.ico"
!define MUI_HEADERIMAGE "..\app\data\modern-header.bmp"
!define MUI_HEADERIMAGE_BITMAP "..\app\data\modern-header.bmp"
!define MUI_WELCOMEFINISHPAGE_BITMAP "..\app\data\modern-wizard.bmp"
!define MUI_UNWELCOMEFINISHPAGE_BITMAP "..\app\data\modern-wizard.bmp"
!define MUI_ABORTWARNING

;Show all languages, despite user's codepage
!define MUI_LANGDLL_ALLLANGUAGES

;--------------------------------
;Language Selection Dialog Settings

;Remember the installer language
!define MUI_LANGDLL_REGISTRY_ROOT "${RegistryCat}"
!define MUI_LANGDLL_REGISTRY_KEY "${RegistryKey}"
!define MUI_LANGDLL_REGISTRY_VALUENAME "Language"

;--------------------------------
;Pages
!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES

!define MUI_FINISHPAGE_RUN "$INSTDIR\${Executable}.exe"

!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_UNPAGE_WELCOME
!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES
!insertmacro MUI_UNPAGE_FINISH

;--------------------------------
;Languages

!insertmacro MUI_LANGUAGE "English" ;first language is the default language
!insertmacro MUI_LANGUAGE "French"
!insertmacro MUI_LANGUAGE "German"
!insertmacro MUI_LANGUAGE "Spanish"

;--------------------------------
;Reserve Files

;If you are using solid compression, files that are required before
;the actual installation should be stored first in the data block,
;because this will make your installer start faster.

!insertmacro MUI_RESERVEFILE_LANGDLL

;--------------------------------
;Installer Sections

; ----------------------------------------
; Directx section
Section "DirectX Install" SEC_DIRECTX
 
  SectionIn RO
 
  SetOutPath "$TEMP"
  File "dxwebsetup.exe"
  DetailPrint "Running DirectX Setup..."
  MessageBox MB_OK|MB_USERICON "Setup will install DirectX"
  ExecWait '"$TEMP\dxwebsetup.exe" /Q' $DirectXSetupError
  DetailPrint "Finished DirectX Setup"
  MessageBox MB_OK|MB_ICONINFORMATION "Finished DirectX Install!"
 
  Delete "$TEMP\dxwebsetup.exe"
 
  SetOutPath "$INSTDIR"
 
SectionEnd

; ----------------------------------------
; VCRedist section
Section "VCRedist Install" SEC_VCREDIST
 
  SectionIn RO
 
  SetOutPath "$TEMP"
  File "vcredist.exe"
  DetailPrint "Running VCRedist Setup..."
  MessageBox MB_OK|MB_USERICON "Setup will install Visual C++ Redist"
  ExecWait '"$TEMP\vcredist.exe" /passive /norestart' $VCRedistSetupError
  DetailPrint "Finished VCRedist Setup"
  MessageBox MB_OK|MB_ICONINFORMATION "Finished Visual C++ Redist Install!"
 
  Delete "$TEMP\vcredist.exe"
 
  SetOutPath "$INSTDIR"
 
SectionEnd

; ----------------------------------------
; Default section
Section
  SetOutPath "$INSTDIR"
  
  ;Client, configuration and misc files
  File /r "${SrcDir}\*"
  ;ExecWait "MicrosoftEdgeWebview2Setup.exe /silent /install"

  ;Delete "$INSTDIR\MicrosoftEdgeWebview2Setup.exe"

  WriteUninstaller "$INSTDIR\uninstall.exe"

  ;Shortcut on desktop
  CreateShortCut "$DESKTOP\${Product}.lnk" '"$INSTDIR\${Executable}.exe"'
  
  CreateDirectory "$SMPROGRAMS\${Company}"
  ;Shortcut on Menu 
  CreateShortCut "$SMPROGRAMS\${Company}\${Product}.lnk" '"$INSTDIR\${Executable}.exe"'
  ; Uninstall Shorcut 
  CreateShortCut "$SMPROGRAMS\${Company}\Uninstall.lnk" '"$INSTDIR\Uninstall.exe"'
  
  ;Store installation folder
  WriteRegStr ${RegistryCat} "${RegistryKey}" "${GenericProduct} Install Path" $INSTDIR
  WriteRegStr ${RegistryCat} "${RegistryWindowsUninstall}\${Product}" "DisplayName" "${Product}"
  WriteRegStr ${RegistryCat} "${RegistryWindowsUninstall}\${Product}" "DisplayIcon" '"$INSTDIR\${Executable}.exe"'
  WriteRegStr ${RegistryCat} "${RegistryWindowsUninstall}\${Product}" "UninstallString" '"$INSTDIR\Uninstall.exe"'

SectionEnd

;--------------------------------
;Installer Functions

Function .onInit
  !insertmacro MUI_LANGDLL_DISPLAY
FunctionEnd

section "uninstall"
	# Shortcut on desktop
	Delete "$DESKTOP\${Product}.lnk"
	Delete "$SMPROGRAMS\${Company}\*.*"
	# Try to remove the Menu shortcut
	RmDir "$SMPROGRAMS\${Company}\"
 
	# Remove files
	RmDir /r "$INSTDIR\*.*"
  
	# Remove uninstaller information from the registry
	DeleteRegKey ${RegistryCat} "${RegistryKey}" 
	DeleteRegKey ${RegistryCat} "${RegistryWindowsUninstall}\${Product}"
sectionEnd


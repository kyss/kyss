#!/usr/bin/env bash
########################################################################
#     ┏┓┏━┓━━━━━━━━━━━━━
#     ┃┃┃┏┛━━━━━━━━━━━━━
#     ┃┗┛┛━┏┓━┏┓┏━━┓┏━━┓
#     ┃┏┓┃━┃┃━┃┃┃━━┫┃━━┫
#     ┃┃┃┗┓┃┗━┛┃┣━━┃┣━━┃
#     ┗┛┗━┛┗━┓┏┛┗━━┛┗━━┛
#     ━━━━━┏━┛┃━━━━━━━━━
#     ━━━━━┗━━┛━━━━━━━━━
#     
# This file is part of the Kyss project (https://gitlab.com/ulukyn/kyss).
# Copyright (c) 2020 Nuno Gonçalves (Ulukyn).
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# ###
# This script will generate the installation packages (an installer.exe) for Windows systems from Linux
# ###


if [[ $1 == "32" ]]
then
	MODE=win32
	WIN_MODE=x32
else
	MODE=amd64
	WIN_MODE=x64
fi

source kyss-dev/unix/utils.sh
source kyss-dev/windows/config.sh

APPNAME=$(grep name setup.cfg | head -n 1 | cut -d" " -f 3 )
KYSS_URL=$(grep name setup.cfg | head -n 1 | cut -d" " -f 3 )

KYSS=kyss-dev/windows
Package_name=$(capitalize_first ${APPNAME})

rm -rf build
mkdir build
cd build 

c $Black$OnYellow "-= CREATING PYTHON FOLDER =-"
cn $Cyan "Downloading https://www.python.org/ftp/python/${PYTHON_FULL_VERSION}/python-${PYTHON_FULL_VERSION}-embed-$MODE.zip..."
curl -s https://www.python.org/ftp/python/${PYTHON_FULL_VERSION}/python-${PYTHON_FULL_VERSION}-embed-$MODE.zip -o python.zip
c $Green "OK"

rm -rf python
mkdir python
cd python

cn $Cyan "Unziping python.zip..."
unzip -q ../python.zip
c $Green "OK"

cn $Cyan "Adding python"${PYTHON_VERS}".zip to python${PYTHON_VERS}._pth ..."
echo "python"${PYTHON_VERS}".zip
.

# Uncomment to run site.main() automatically
import site
" > python${PYTHON_VERS}._pth
c $Green "OK"

c $Black$OnYellow "-= INSTALLING PIP =-"

c $Cyan "Installing pip ..."
curl -s https://bootstrap.pypa.io/get-pip.py -o get-pip.py
wine python.exe get-pip.py --no-warn-script-location 2> /dev/null
c $Green "OK"

c $Cyan "Installing tkinter ..."
curl -s https://gitlab.com/kyss/kyss/-/raw/dev/files/tkinter-3.8.zip?inline=false --output tkinter.zip
unzip tkinter.zip
rm -rf tkinter.zip
c  $Green "OK"

CURRENT_PATH=$(pwd)
c $Black$OnYellow "-= INSTALLING KYSS =-"

#wine python.exe -m pip install $KYSS_URL --no-warn-script-location
wine python.exe -m pip install urllib3 --no-warn-script-location
wine python.exe -m pip install certifi --no-warn-script-location
wine python.exe -m pip install pywin32 --no-warn-script-location
#wine python.exe -m pip install pillow --no-warn-script-location


cd ..
rm -rf APP
mkdir APP

c $Black$OnYellow "-= COPYING DATA =-"
mv python APP/python
mkdir APP/app
cp -r ../app/data APP/app/
cp -r ../app/modules APP/app/
cp -r ../app/VERSION.txt APP/app/
mkdir APP/app/site-packages/
convert APP/app/data/icon.png -define icon:auto-resize=64,48,32,16 APP/app/data/icon.ico
convert APP/app/data/icon_install.png -define icon:auto-resize=64,48,32,16 APP/app/data/icon_install.ico
convert APP/app/data/icon_uninstall.png -define icon:auto-resize=64,48,32,16 APP/app/data/icon_uninstall.ico
cp ../${APPNAME}.py APP
cp ../kyss.cfg APP
cp ../setup.cfg APP

c $Black$OnYellow "-= CREATING START EXE =-"
makensis -NOCD -DGenericProduct=${Package_name} -DScript=${APPNAME} -DExecutable=${Package_name}.exe ../${KYSS}/nsis/start.nsi
mv ${Package_name}.exe APP

c $Black$OnYellow "-= CREATING INSTALL EXE =-"

USE_DIRECTX=$(grep directx ../setup.cfg)

if [ ! -z "$USE_DIRECTX" ]
then
	cn $Cyan "Downloading DirectX..."
	curl -s  https://download.microsoft.com/download/1/7/1/1718CCC4-6315-4D8E-9543-8E28A4E18C4C/dxwebsetup.exe -o dxwebsetup.exe
	
	DIRECTX=$(cat << EOF
; ----------------------------------------
; Directx section
Section "DirectX Install" SEC_DIRECTX
 
  SectionIn RO
 
  SetOutPath "\$TEMP"
  File "dxwebsetup.exe"
  DetailPrint "Running DirectX Setup..."
  MessageBox MB_OK|MB_USERICON "Setup will install DirectX"
  ExecWait '"\$TEMP\dxwebsetup.exe" /Q' \$DirectXSetupError
  DetailPrint "Finished DirectX Setup"
  MessageBox MB_OK|MB_ICONINFORMATION "Finished DirectX Install!"
 
  Delete "\$TEMP\dxwebsetup.exe"
 
  SetOutPath "\$INSTDIR"
 
SectionEnd
EOF
	)

	DIRECTX=${DIRECTX//$'\n'/\\\\n}
	DIRECTX=$(echo $DIRECTX | sed "s/\"/\\\\\"/g")
	DIRECTX=$(echo $DIRECTX | sed "s/\//\\\/g")
	sed -i -e "s/; --- EXTRA ---/$DIRECTX\\n\\n; --- EXTRA ---/g" install.nsi
	sed -i -e "s/\\\\n/\\n/g" install.nsi
	
	c $Green "OK"
fi

USE_VCREDIST=$(grep vcredist ../setup.cfg)

if [ ! -z "$USE_VCREDIST" ]
then
	cn $Cyan "Downloading VC++ Redist..."
	curl -s -L https://aka.ms/vs/17/release/vc_redist.${WIN_MODE}.exe -o vcredist.exe
	c $Green "OK"
	VCREDIST=$(cat << EOF
; ----------------------------------------
; VCRedist section
Section "VCRedist Install" SEC_VCREDIST
 
  SectionIn RO
 
  SetOutPath "$TEMP"
  File "vcredist.exe"
  DetailPrint "Running VCRedist Setup..."
  MessageBox MB_OK|MB_USERICON "Setup will install Visual C++ Redist"
  ExecWait '"$TEMP\vcredist.exe" /passive /norestart' $VCRedistSetupError
  DetailPrint "Finished VCRedist Setup"
  MessageBox MB_OK|MB_ICONINFORMATION "Finished Visual C++ Redist Install!"
 
  Delete "$TEMP\vcredist.exe"
 
  SetOutPath "$INSTDIR"
 
SectionEnd
EOF
)

	VCREDIST=${VCREDIST//$'\n'/\\\\n}
	VCREDIST=$(echo $VCREDIST | sed "s/\"/\\\\\"/g")
	VCREDIST=$(echo $VCREDIST | sed "s/\//\\\/g")
	sed -i -e "s/; --- EXTRA ---/$VCREDIST\\n\\n; --- EXTRA ---/g" install.nsi
	sed -i -e "s/\\\\n/\\n/g" install.nsi
fi

makensis -NOCD -DGenericProduct=${Package_name} -DExecutable=${Package_name} -DDataDir=../${KYSS}/nsis/ ../${KYSS}/nsis/install.nsi

mkdir -p ../dist/
mv ${Package_name}Installer.exe ../dist/${APPNAME}_installer_${WIN_MODE:1}.exe

c $Black$OnGreen "-= DONE ! =-"

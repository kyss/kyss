rmdir /q /s build
mkdir build
cd build
curl https://www.python.org/ftp/python/3.8.6/python-3.8.6-embed-amd64.zip --output python.zip
echo Expand-Archive python.zip > extract.ps1
Powershell.exe -executionpolicy remotesigned -File extract.ps1

curl https://me.ryzom.com/tk.zip --output tk.zip
echo Expand-Archive tk.zip . > extract.ps1
Powershell.exe -executionpolicy remotesigned -File extract.ps1

copy ..\python38_pre._pth python\python38._pth

xcopy /q /s /e /y tk\tcl python\tcl\
xcopy /q /s /e /y tk\tkinter python\Lib\site-packages\tkinter\
xcopy /q /s /e /y tk\_tkinter.pyd python\
xcopy /q /s /e /y tk\tcl86t.dll python\
xcopy /q /s /e /y tk\tk86t.dll python\

curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python\python get-pip.py

cd python
Scripts\pip install -r ../../requirements.txt

mkdir python38
cd python38
..\..\..\7z.exe x ..\python38.zip

for /f %%i in ('dir /b ..\Lib\site-packages\*') do move ..\Lib\site-packages\%%i site-packages\%%i

copy ..\..\..\humanize__init__.py site-packages\humanize\__init__.py

for /f %%i in ('dir /a:d /b site-packages\Pillow*') do move site-packages\%%i ..\Lib\site-packages\
for /f %%i in ('dir /a:d /b site-packages\certifi*') do move site-packages\%%i ..\Lib\site-packages\
for /f %%i in ('dir /a:d /b site-packages\wheel*') do move site-packages\%%i ..\Lib\site-packages\
for /f %%i in ('dir /a:d /b site-packages\pip*') do move site-packages\%%i ..\Lib\site-packages\
for /f %%i in ('dir /a:d /b site-packages\setuptools*') do move site-packages\%%i ..\Lib\site-packages\
move site-packages\PIL ..\Lib\site-packages\PIL
cd ..
copy ..\..\pylnk.py Scripts\pylnk.py
copy ..\..\python38_post._pth python38._pth
del python38.zip

cd python38
..\..\..\zip.exe -r ..\python38.zip .
cd ..

rmdir /q /s python38
pause

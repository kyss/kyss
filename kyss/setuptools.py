########################################################################
#     ┏┓┏━┓━━━━━━━━━━━━━
#     ┃┃┃┏┛━━━━━━━━━━━━━
#     ┃┗┛┛━┏┓━┏┓┏━━┓┏━━┓
#     ┃┏┓┃━┃┃━┃┃┃━━┫┃━━┫
#     ┃┃┃┗┓┃┗━┛┃┣━━┃┣━━┃
#     ┗┛┗━┛┗━┓┏┛┗━━┛┗━━┛
#     ━━━━━┏━┛┃━━━━━━━━━
#     ━━━━━┗━━┛━━━━━━━━━
#     
# This file is part of the Kyss project (https://gitlab.com/ulukyn/kyss).
# Copyright (c) 2020 Nuno Gonçalves (Ulukyn).
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import os
import shutil
import platform
import setuptools
from pathlib import Path
from pkg_resources import resource_filename, require
from setuptools.command.develop import develop
from setuptools.command.install import install

from kyss.utils import KyssUtils
ku = KyssUtils("KyssSetupTools")

print(ku.c("bggreen")+ku.c("black"))
print(ku.style("bright")+" "*10+"🔱 Kyss SetupTools 🔱"+ku.style("normal")+ku.c("reset")+ku.c("bgreset"))
print()
	

def get_version(package):
	ku.pcheck("Getting {} version".format(package))
	try:
		return require(package)[0].version
	except:
		ku.pdone("No module found")
		return 0

def setup(PACKAGE_NAME, PACKAGE_VERSION, PACKAGE_DESC, REQUIRE, URL, DOWNLOAD, AUTHOR, EMAIL, LICENSE, KEYWORDS=[], CLASSIFIERS=[]):
	with open("README.md") as fh:
		long_description = fh.read()

	install_requires = []
	extras_require = {}
	cmdclass = {}
	
	for cat, packages in REQUIRE.items():
		if platform.system() == cat or cat == "*":
			install_requires += packages

	version = get_version(PACKAGE_NAME)
	
	if version != PACKAGE_VERSION:
		cmdclass = {"develop": PostDevelopCommand, "install": PostInstallCommand}
	else:
		cmdclass = {"develop": PostDevelopCommand}

	ku.pexec("Setuping {}".format(PACKAGE_NAME))
	setuptools.setup(
		name=PACKAGE_NAME,
		author=AUTHOR,
		author_email=EMAIL,
		description=(PACKAGE_DESC),
		long_description=long_description,
		long_description_content_type="text/markdown",
		url=URL,
		download_url=DOWNLOAD,
		keywords=KEYWORDS,
		install_requires=install_requires,
		extras_require=extras_require,
		version=PACKAGE_VERSION,
		include_package_data=True,
		packages=[PACKAGE_NAME],
		package_dir={PACKAGE_NAME : PACKAGE_NAME},
		package_data={PACKAGE_NAME: ["data/*"]},
		license=LICENSE,
		license_files = ("LICENSE.txt",),
		classifiers=[
			"Intended Audience :: Developers",
			"License :: OSI Approved :: BSD License",
			"Operating System :: OS Independent",
			"Environment :: MacOS X",
			"Environment :: Win32 (MS Windows)",
			"Environment :: X11 Applications :: GTK",
			"Environment :: X11 Applications :: Qt",
			"Programming Language :: Python",
			"Programming Language :: Python :: 3",
			"Programming Language :: Python :: 3.4",
			"Programming Language :: Python :: 3.5",
			"Programming Language :: Python :: 3.6",
			"Programming Language :: Python :: 3.7",
			"Programming Language :: Python :: 3.8",
			"Programming Language :: Python :: 3.9",
			"Topic :: Software Development :: Libraries :: Application Frameworks",
			"Topic :: Software Development :: Libraries :: Python Modules",
			"Topic :: Software Development :: User Interfaces"
		]+CLASSIFIERS,
		cmdclass=cmdclass,
	)
	ku.pdone("All done")


def install_data(srcpath, kysspath):
	ku.pexec("Installing data")
	if not Path(kysspath+"/www").is_dir():
		os.makedirs(str(Path(kysspath+"/www")))
	for fname in os.listdir(srcpath):
		if not Path(kysspath+"/data/"+fname).is_file():
			shutil.move(str(Path(srcpath+"/"+fname)), str(Path(kysspath+"/www/"+fname)))
	ku.pdone()

class PostDevelopCommand(develop):
	"""Post-installation for development mode."""
	def run(self):
		develop.run(self)
		install_data(resource_filename("kyss-webview", "data"), "../..")

class PostInstallCommand(install):
	"""Post-installation for installation mode."""
	def run(self):
		install.run(self)
		install_data(resource_filename("kyss-webview", "data"), "../..")
		

########################################################################
#     ┏┓┏━┓━━━━━━━━━━━━━
#     ┃┃┃┏┛━━━━━━━━━━━━━
#     ┃┗┛┛━┏┓━┏┓┏━━┓┏━━┓
#     ┃┏┓┃━┃┃━┃┃┃━━┫┃━━┫
#     ┃┃┃┗┓┃┗━┛┃┣━━┃┣━━┃
#     ┗┛┗━┛┗━┓┏┛┗━━┛┗━━┛
#     ━━━━━┏━┛┃━━━━━━━━━
#     ━━━━━┗━━┛━━━━━━━━━
#     
# This file is part of the Kyss project (https://gitlab.com/ulukyn/kyss).
# Copyright (c) 2020 Nuno Gonçalves (Ulukyn).
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import os
import sys
import shutil
import tkinter as tk
from tkinter import filedialog, StringVar
from PIL import Image, ImageTk
from kyss.progress.tk import KyssTkProgressBar
from kyss.module import KyssModule
from .utils import *

class KyssInstallerLinux(tk.Tk, KyssModule):

	def __init__(self, params=None):
		KyssModule.__init__(self)
		tk.Tk.__init__(self)
		tk.Tk.option_add(self, "*TkChooseDir*foreground", "#ffffff")
		tk.Tk.option_add(self, "*TkChooseDir*background", "#222222")
		self.progress = None
		default_params = {
			"geometry": "600x580",
			"bg": "black",
			"bg_image": "data/installer.jpg",
			"welcome_text2": "",
		}
		if not "name" in params:
			params["name"] = "My Kyss App"
		default_params["title"] = self._("title", (params["name"]))
		default_params["welcome_text1"] = self._("welcome", (params["description"]))
		default_params["install_text"] = self._("installing", (params["name"]))


		if params:
			self.params = params
		else:
			self.params = default_params
		
		for n,v in default_params.items():
			if not n in self.params:
				self.params[n] = v

	def report_callback_exception(self, exc, val, tb):
		sys.excepthook(exc, val, tb)
		os._exit(1)
		
	def _findXCenter(self, canvas, item):
		coords = canvas.bbox(item)
		# TODO: use geometry params for width
		return 300 - ((coords[2] - coords[0]) / 2)

	def _clearFrame(self):
		for widget in self.frame.winfo_children():
			if widget == self.progress:
				widget.forget()
			else:
				widget.destroy()
			
	def create(self):
		self.title(self.params["title"])

		self.geometry(self.params["geometry"])
		self.configure(bg=self.params["bg"])

		# Image
		img = ImageTk.PhotoImage(Image.open(self.params["bg_image"]))
		label = tk.Label(self, image=img, bd=0)
		label.image = img
		label.pack(side="top")
		
		# Text
		canvas = tk.Canvas(width=600, height=80, highlightthickness=0, bg="#000")
		canvas.pack(side="top")
		lbl = canvas.create_text(0, 0, font="Helvetica 24", fill="orange", anchor=tk.NW)
		canvas.insert(lbl, 24, self.params["welcome_text1"])
		xOffset = self._findXCenter(canvas, lbl)
		canvas.move(lbl, xOffset, 10)

		lbl = canvas.create_text(0, 0, font="Helvetica 16", fill="#bbb", anchor=tk.NW)
		canvas.insert(lbl, 24, self.params["welcome_text2"])
		xOffset = self._findXCenter(canvas, lbl)
		canvas.move(lbl, xOffset, 50)
		
		self.canvas = canvas
		
		self.frame = tk.Frame(self, bg="#222")
		self.frame.pack(side="top", fill="both", expand=True)

	def createProgressBar(self, text=""):
		self._clearFrame()

		label_text = StringVar()

		if not text:
			label_text.set(self.params["install_text"])
		else:
			label_text.set(text)

		label = tk.Label(self.frame, textvariable=label_text, fg="white", bg="#222", bd=0)
		label.pack(side="top", expand=True)
		
		self.progress = KyssTkProgressBar(self, 535, 30)#, "#07b")
		self.progress.pack(side="top", fill="both", expand=True)
		self.progress.label = label_text

		frame = tk.Frame(self.frame, bg="#070707")
		frame.pack(side="top", fill="both", expand=True)

		but = tk.Button(frame, text=self._("cancel"), command=lambda: sys.exit())
		but.pack(padx="10", side="right")
		
		return self.progress


	def startInstallation(self, installer):
		self.installer = installer
		self._clearFrame()
		
		label = tk.Label(self.frame, height=4, text=self._("start_install"), fg="white", bg="#222", bd=0)
		label.pack(side="top", expand=True)

		frame = tk.Frame(self.frame, bg="#070707")
		frame.pack(side="top", fill="both", expand=True)

		but = tk.Button(frame, text=self._("next"), command=lambda: self.startFileChoose())
		but.pack(padx="10", side="right")

		but = tk.Button(frame, text=self._("cancel"), command=lambda: sys.exit())
		but.pack(padx="10", side="right")
		self.mainloop()
	
	def startFileChoose(self):
		self._clearFrame()

		label = tk.Label(self.frame, text=self._("choose_path", (self.params["name"])), fg="white", bg="#222", bd=0)
		label.pack(side="top", expand=True)

		frame = tk.Frame(self.frame, bg="#222")
		frame.pack(side="top", fill="both", padx="10", expand=True)

		entry = tk.Entry(frame, fg="white", bg="#222")
		entry.insert(0, os.path.expanduser("~/"+self.params["name"]))
		entry.pack(side="left", fill="both", pady=10, expand=True)

		def openFileChooser(entry):
			initialdir = entry.get()
			if not os.path.isdir(initialdir):
				initialdir = os.sep.join(os.path.split(initialdir)[:-1])
				
			folder = filedialog.askdirectory(parent=self, initialdir=initialdir, title=self._("select"), mustexist=tk.TRUE)
			entry.delete(0, "end")
			if folder:
				if os.path.basename(folder) != self.params["name"]:
					folder += os.sep+self.params["name"]
				entry.insert(0, folder)
			else:
				entry.insert(0, os.path.expanduser("~/"+self.params["name"]))
		
		openFileChooser(entry)
		
		but = tk.Button(frame, text=self._("select"), command=lambda: openFileChooser(entry))
		but.pack(padx="10", side="right")

		frame = tk.Frame(self.frame, bg="#070707")
		frame.pack(side="top", fill="both", expand=True)
		
		but = tk.Button(frame, text=self._("next"), command=lambda: self.callInstaller(entry.get()))
		but.pack(padx="10", side="right")

		but = tk.Button(frame, text=self._("cancel"), command=lambda: sys.exit())
		but.pack(padx="10", side="right")

	def callInstaller(self, install_dir):
		self.install_dir = install_dir
		progress = self.createProgressBar()
		self.installer(progress, install_dir)

	def finish(self):
		self._clearFrame()
		
		label = tk.Label(self.frame, text=self._("finish"), fg="white", bg="#222", bd=0)
		label.pack(side="top", expand=True)
		label = tk.Label(self.frame, text="\o/   "+self.params["name"].upper()+" "+self._("installed")+"   \o/", fg="orange", bg="#222", bd=0)
		label.pack(side="top", expand=True)

		frame = tk.Frame(self.frame, bg="#070707")
		frame.pack(side="top", fill="both", expand=True)

		but = tk.Button(frame, text=self._("quit"), command=lambda: self.destroy())
		but.pack(padx="10", side="right")


	def _removetree(self, folder):
		def remove_readonly(func, path, _):
			os.chmod(path, stat.S_IWRITE)
			func(path)

		try:
			shutil.rmtree(folder, onerror=remove_readonly)
		except:
			pass

	def _copytree(self, src, dst, symlinks=False, ignore=None):
		if not os.path.exists(dst):
			os.makedirs(dst)
		for item in os.listdir(src):
			s = os.path.join(src, item)
			d = os.path.join(dst, item)
			if os.path.isdir(s):
				self._copytree(s, d, symlinks, ignore)
			else:
				self.files_to_copy.append((s, d))

	def _fullCopyOrCurDir(self, progress, install_dir):
		self.files_to_copy = []
		self._copytree(os.path.abspath(os.getcwd()+"/.."), install_dir)
		total_files = len(self.files_to_copy)
		i = 0
		for s, d in self.files_to_copy:
			i += 1
			if "/cache/" in s:
				continue
			progress.setValue(round((100*i)/total_files), "Installing {}...".format(os.path.basename(s)))
			shutil.copy2(s, d)

	def install(self, progress, install_dir=""):
		if os.path.isdir("../python"):
			self.log("Install in {}".format(install_dir))
			self._removetree(install_dir+"/app")
			self._removetree(install_dir+"/python")
			self._fullCopyOrCurDir(progress, install_dir)
			with open(install_dir+"/Ryztart.desktop", "r") as f:
				desktop_file = f.read().replace("%", install_dir)
			with open(install_dir+"/Ryztart.desktop", "w") as f:
				f.write(desktop_file)

			try:
				subprocess.run(["xdg-desktop-menu", "install", "--novendor", install_dir+"/Ryztart.desktop"])
			except:
				self.error("Ryztart.desktop not installed")

			try:
				subprocess.run(["xdg-desktop-icon", "install", "--novendor", install_dir+"/Ryztart.desktop"])
			except:
				self.error("Ryztart.desktop icon not installed in desktop")

			ryztart_file = "#!/bin/sh\n\
cd {root}\n\
export LD_LIBRARY_PATH={root}/python/usr/lib:{root}/python/opt/python{py}/lib/python{py}/site-packages/PyQt5/Qt5/lib/:.\n\
APPIMAGE=. APPDIR={root}/python python/AppRun -X pycache_prefix=cache ryztart.py \"$@\"".format(root=install_dir, py=python_version)
			ryztart_file_name = install_dir+"/ryztart"
			with open(ryztart_file_name, "w") as f:
				f.write(ryztart_file)
			os.chmod(ryztart_file_name, 0o775)
			uninstall_file = "#!/bin/sh\n\
cd {root}\n\
export LD_LIBRARY_PATH={root}/python/usr/lib:{root}/python/opt/python{py}/lib/python{py}/site-packages/PyQt5/Qt5/lib/:.\n\
APPIMAGE=. APPDIR={root}/python python/AppRun -X pycache_prefix=cache ryztart.py --uninstall 1".format(root=install_dir, py=python_version)
			uninstall_file_name = install_dir+"/uninstall"
			with open(uninstall_file_name, "w") as f:
				f.write(uninstall_file)
			os.chmod(uninstall_file_name, 0o775)
			os.chdir(install_dir)
			self.log("Please close and start Ryztart...")

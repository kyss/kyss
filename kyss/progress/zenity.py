########################################################################
#     ┏┓┏━┓━━━━━━━━━━━━━
#     ┃┃┃┏┛━━━━━━━━━━━━━
#     ┃┗┛┛━┏┓━┏┓┏━━┓┏━━┓
#     ┃┏┓┃━┃┃━┃┃┃━━┫┃━━┫
#     ┃┃┃┗┓┃┗━┛┃┣━━┃┣━━┃
#     ┗┛┗━┛┗━┓┏┛┗━━┛┗━━┛
#     ━━━━━┏━┛┃━━━━━━━━━
#     ━━━━━┗━━┛━━━━━━━━━
#     
# This file is part of the Kyss project (https://gitlab.com/ulukyn/kyss).
# Copyright (c) 2020 Nuno Gonçalves (Ulukyn).
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import os
import subprocess
from kyss.utils import KyssUtils

k = KyssUtils("KyssZenityProgress")
k.log("Trying zenity...")

if os.path.isfile("../zenity.exe"):
	zenity_bin = "../zenity.exe"
else:
	zenity_bin = "zenity"

try:
	result = subprocess.run([zenity_bin, "--help"], capture_output=True)
	k.log("Zenity available")
	zenityAvailable = True
except:
	k.warning("Zenity not available")
	zenityAvailable = False
	
if zenityAvailable:
	class KyssZenityProgress():
		def __init__(self, callback, params=""):
			self.p = subprocess.Popen([zenity_bin, "--progress", "--percentage=0", "--text=Updating...", "--auto-close=1", "--title=Kyss Self Updater"], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
			callback(self, params)
		
		def setValue(self, value, message=""):
			self.p.stdin.write(("%d\n" % value).encode())
			self.p.stdin.write(("# %s\n" % message).encode())
			try:
				self.p.stdin.flush()
			except:
				pass
			
			print(k.c("green")+"{}%".format(value)+k.c("green"), message, k.c("reset"))
		
		def finish(self):
			pass
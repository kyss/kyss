########################################################################
#     ┏┓┏━┓━━━━━━━━━━━━━
#     ┃┃┃┏┛━━━━━━━━━━━━━
#     ┃┗┛┛━┏┓━┏┓┏━━┓┏━━┓
#     ┃┏┓┃━┃┃━┃┃┃━━┫┃━━┫
#     ┃┃┃┗┓┃┗━┛┃┣━━┃┣━━┃
#     ┗┛┗━┛┗━┓┏┛┗━━┛┗━━┛
#     ━━━━━┏━┛┃━━━━━━━━━
#     ━━━━━┗━━┛━━━━━━━━━
#     
# This file is part of the Kyss project (https://gitlab.com/ulukyn/kyss).
# Copyright (c) 2021 Nuno Gonçalves (Ulukyn).
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

#
# This setup file are only used by Macos
# It's required since pip install don't works (no idea why...)
#


import os
import configparser
from setuptools import setup

config = configparser.ConfigParser()
config.read("setup.ini")

if __name__ == "__main__":
	setup(
		name = config["metadata"]["name"],
		description = config["metadata"]["description"],
		url = config["metadata"]["url"],
		version = config["metadata"]["version"],
		author = config["author"]["name"]+" ("+config["author"]["email"]+")",
		long_description = "file: README.md",
		keywords = "pywebview, build, packaging",
		package_dir = {"kyss": "kyss"},
		packages = ["kyss", "kyss.progress"],
	)
